#!/usr/bin/python

import sys, os
import numpy as np
import cPickle
import cv2

names_prefix = 'data_batch_'
path = './cifar-10-batches-py/'

def unpickle(file):
    fo = open(file, 'rb')
    dict = cPickle.load(fo)
    fo.close()
    return dict

def convert_binary_data(dict,post_prefix):
	images = dict['data']
	names = dict['filenames']
	labels = dict['labels']
	batch_label = dict['batch_label']

	batch_label = batch_label.replace(' ','_')
	# print out image list and labels
	fid = open(batch_label+'.txt','w')
	for j in range(0,len(labels)):
		fid.write('%s %d\n' % (names[j],labels[j]))
	fid.close()

	# save data into images
	if os.path.exists(path+'image_batch_'+post_prefix) is False:
		os.mkdir(path+'image_batch_'+post_prefix)

	for j in range(0,len(labels)):
		im = dict['data'][j]
		im_res = np.resize(im,[3,32,32])
		im_res = im_res.swapaxes(0,2)
		im_res = im_res.swapaxes(0,1)

		cv2.imwrite(path+'image_batch_'+post_prefix+'/'+names[j],im_res)


if __name__ == '__main__':
    # unpicking 
	name = 'test_batch'
	dict = unpickle(path+name)
	convert_binary_data(dict,'test')

	exit()

	# unpicking training data
	for i in range(1,6):
		name = names_prefix + str(i)
		dict = unpickle(path+name)
		convert_binary_data(dict, str(i))







