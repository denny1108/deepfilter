#!/usr/bin/python/
# import caffe
import sys, os
import cv2
import numpy as np
import h5py

from scipy import ndimage as ndi
import matplotlib.pyplot as plt

prefix = '/home/Jingjing/Documents/research/caffe/data/cifar10/cifar-10-batches-py'

def output_sobel(img, sz_filter):
	# sobel filters: combined, x-axis, y-axis
	sobel_x = cv2.Sobel(img,cv2.CV_64F,1,0,ksize=sz_filter)
	sobel_y = cv2.Sobel(img,cv2.CV_64F,0,1,ksize=sz_filter)
	sobel = cv2.Sobel(img,cv2.CV_64F,1,1,ksize=sz_filter)

    # absolute value of sobel outputs
	sobel_x_abs = np.absolute(sobel_x)
	sobel_y_abs = np.absolute(sobel_y)
	sobel_abs = np.absolute(sobel)

    # stack sobel outputs
	# sobel_ou = np.dstack((sobel,sobel_x,sobel_y,sobel_abs,sobel_x_abs,sobel_y_abs))
	sobel_ou = np.dstack((sobel,sobel_x,sobel_y))
	return sobel_ou


def output_laplacian(img, sz_filter):
	#laplacian fitler:
	laplacian = cv2.Laplacian(img,cv2.CV_64F,ksize=sz_filter)

	# absolute value of laplacian output
	laplician_abs = np.absolute(laplacian)

	# stack laplacian outputs
	# laplacian_ou = np.dstack((laplacian,laplician_abs))
	laplacian_ou = laplacian
	return laplacian_ou


def gabor_filter_bank(sz_filter):
	filters = []
	ksize = sz_filter
	# generate gabor filters
	for theta in np.arange(0, np.pi, np.pi / 8):
		for lamda in np.arange(np.pi/4, np.pi, np.pi/4): 
			kern = cv2.getGaborKernel((ksize, ksize), 1, theta, lamda, np.pi/4, 0, ktype=cv2.CV_32F)
			kern /= 1.5*kern.sum()
			filters.append(kern)

	return filters

def show_gabor_filter(filters):
	plt.figure()
	num_col = 4
	num_row = np.ceil((1.0*len(filters)/num_col))
	for i in range(len(filters)):
		plt.subplot(num_row,num_col,i+1)
		# plt.title("gabor filter #{}".format(i+1))
		plt.imshow(filters[i])
		plt.axis('off')
	plt.show()


def output_gabor(img, filters):
	gabor_ou = np.array([])
	# implement convolution operation
	for kernel in filters:
		gabor = cv2.filter2D(img,cv2.CV_32F,kernel)
		if gabor_ou.size == 0:
			gabor_ou = gabor
		else:
			gabor_ou = np.dstack((gabor_ou,gabor))

	return gabor_ou

def show_output(results):
	num_col = 4
	num_row = np.ceil((1.0*results.shape[2]/num_col))

	plt.figure()
	for i in range(0,results.shape[2]):
		plt.subplot(num_row,num_col,i+1)
		plt.title("filter #{} output".format(i+1))
		plt.imshow(results[:,:,i], cmap='gray')
		plt.axis('off')
	plt.show()

def test(img_name):
	img = cv2.imread(img_name,0)
	if img is None:
		print 'Image is not read successfully.'
		exit()
	# sobel filter and show results
	sobel_o = output_sobel(img,5)
	# show_output(sobel_o)

	# laplacian filter and show results
	laplacian_o = output_laplacian(img,5)
	# show_output(laplacian_o)

	# gabor filter and show results
	filters = gabor_filter_bank(5)
	gabor_o = output_gabor(img, filters)
	#show_output(gabor_o)

	channel = sobel_o.shape[2]+1+gabor_o.shape[2]
	return channel

#def binaryproto_to_array():
#	blob = caffe.proto.caffe_pb2.BlobProto()
#	data = open('./caffe/examples/cifar10/mean.binaryproto', 'rb' ).read()
#	blob.ParseFromString(data)
#	arr = np.array( caffe.io.blobproto_to_array(blob) )
#	out = arr[0]
#	out = np.swapaxes(out,0,2)
#	out = np.swapaxes(out,0,1)
#	cv2.imwrite('mean.jpg',out)
#	# print np.amax(out)
#	# print np.amin(out)
#	gray_out = cv2.imread('mean.jpg',0)
#	# convert to grayscale image
#	# gray_out = cv2.cvtColor(out, cv2.COLOR_BGR2GRAY)
#	os.remove('mean.jpg')
#	return gray_out


def images_to_hdf5(fname,channel,post_prefix):
	fid = open(fname)
	name_list = []
	label_list = []
	for line in fid:
	    name_list.append(line.replace('\n','').split()[0])
	    label_list.append(line.replace('\n','').split()[1])

	data = np.zeros((len(name_list),channel,32,32))
	label = np.zeros((len(name_list),1,1,1))
	# mean_img = binaryproto_to_array()

	gabor_filter = gabor_filter_bank(5)

	for k in range(len(name_list)):
	    img = cv2.imread(prefix+'/'+'image_batch_'+post_prefix+'/'+name_list[k],0)
	    # img = img - mean_img
	    sobel_o = output_sobel(img,5)
	    laplacian_o = output_laplacian(img,5)
	    gabor_o = output_gabor(img, gabor_filter)
	    temp = np.dstack((sobel_o,laplacian_o,gabor_o))

	    temp = np.swapaxes(temp, 0,2)
	    temp = np.swapaxes(temp, 1,2)

	    #temp = np.swapaxes(img,0,2)
	    #temp = np.swapaxes(temp,1,2)
	    data[k,:,:,:] = temp
	    label[k,0,0,0] = label_list[k]


	data = data.astype(np.float32)
	label = label.astype(np.float32)

	with h5py.File(prefix + '/cifar10_batch_'+post_prefix+'.h5', 'w') as f:
		f['data'] = data
		f['label'] = label


if __name__ == "__main__":
	
	channel = test('Lenna.png')

	fname = prefix + '/' + 'testing_batch_1_of_1.txt'
	images_to_hdf5(fname,channel,'test')

	for i in range(1,6):
	    fname  = prefix + '/' + 'training_batch_'+str(i)+'_of_5.txt'
	    images_to_hdf5(fname,channel,str(i))





