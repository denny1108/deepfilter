#/usr/bin/python

import sys, os
import numpy as np
import h5py

import matplotlib.pyplot as plt

def show_cnn_filter(filters):

	num_col = 4
	num_row = np.ceil((1.0*len(filters)/num_col))
	plt.figure(1)
	plt.title('display filter: channels 0')
	for i in range(len(filters)):
		plt.subplot(num_row,num_col,i+1)
		# plt.title("gabor filter #{}".format(i+1))
		plt.imshow(filters[i][:,:,0],cmap='gray')
		plt.axis('off')
	plt.show()

	plt.figure(2)
	plt.title('display filter: channels 1')
	for i in range(len(filters)):
		plt.subplot(num_row,num_col,i+1)
		# plt.title("gabor filter #{}".format(i+1))
		plt.imshow(filters[i][:,:,1],cmap='gray')
		plt.axis('off')
	plt.show()

	plt.figure(3)
	plt.title('display filter: channels 2')
	for i in range(len(filters)):
		plt.subplot(num_row,num_col,i+1)
		# plt.title("gabor filter #{}".format(i+1))
		plt.imshow(filters[i][:,:,2],cmap='gray')
		plt.axis('off')
	plt.show()


if __name__ == '__main__':

    # get filter data
	fname = sys.argv[1]
	f = h5py.File(fname,'r')
	conv1_w = f['data']['conv1']['0'][...]

	temp = np.swapaxes(conv1_w,1,3)
	temp = np.swapaxes(temp,1,2)

	# display fiters
	filters = []
	for i in range(temp.shape[0]):
		filters.append(temp[i,:,:,:])

	show_cnn_filter(filters)

	exit()


