#!/usr/bin/python

import sys, os
import numpy as np
from matplotlib import pyplot as plt

def draw_log(train_loss, train_iter, test_accuracy, test_iter):
	assert(len(train_loss)==len(train_iter))
	assert(len(test_accuracy)==len(test_iter))

	tr_loss = np.array(train_loss)
	tr_iter = np.array(train_iter)
	te_accu = np.array(test_accuracy)
	te_iter = np.array(test_iter)

	plt.figure(figsize=(10,5))
	plt.subplot(1,2,1)
	plt.title('training loss')
	plt.plot(tr_iter, tr_loss, 'r-')
	plt.ylim(0, 2)

	plt.subplot(1,2,2)
	plt.title('testing accuracy')
	plt.plot(te_iter, te_accu, 'b-')
	plt.ylim(0, 1)

	plt.show()


if __name__ == '__main__':
	flog = open(sys.argv[1],'r')

	train_loss = []
	train_iter = []
	test_accuracy = []
	test_iter = []

	for lines in flog:
		if lines.find('Iteration ') > 0:
			if lines.find('loss = ') > 0:
				itr = int(lines.split()[5][0:-1])
				loss = float(lines.split()[8])
				train_iter.append(itr)
				train_loss.append(loss)
			if lines.find('Testing net') > 0:
				itr = int(lines.split()[5][0:-1])
				test_iter.append(itr)

		if lines.find('accuracy = ') > 0:
			accu = float(lines.split()[10])
			test_accuracy.append(accu)

	draw_log(train_loss, train_iter, test_accuracy, test_iter)
