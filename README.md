# *DeepFilter* 

by Jingjing Liu

### Introduction

0. cifar10_to_images.py
	- convert binary files of cifar10 data into RGB images
0. cifar10_filter_hdf.py
	- apply [Sobel](https://en.wikipedia.org/wiki/Sobel_operator), [laplacian](https://en.wikipedia.org/wiki/Laplace_operator), and [Gabor](https://en.wikipedia.org/wiki/Gabor_filter) filters to images
	- output data in format of hdf5
0. parse_log.py
	- parse training loss and testing accuray of caffe output log
0. parse_cnn_filters.py
	- parse and display the first convolutional layer